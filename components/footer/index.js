import React from "react";
import { Button, Row, Col, Container, Navbar } from 'react-bootstrap';

function Footer(){
 return(
  <footer className="footer-area black-bg-2 pt-70">
  <div className="footer-top-area pb-18">
    <Container>
      <Row>
        <Col lg={4} md={6} sm={6}>
          <div className="footer-about mb-40">
            <div className="footer-logo">
              <a href="index.html">
                <img src="assets/img/logo/footer-logo.png" alt="" />
              </a>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidi ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
            <div className="payment-img">
              <a href="#">
                <img src="assets/img/icon-img/payment.png" alt="" />
              </a>
            </div>
          </div>
        </Col>
        <Col lg={4} md={6} sm={6}>
          <div className="footer-widget mb-40">
            <div className="footer-title mb-22">
              <h4>Information</h4>
            </div>
            <div className="footer-content">
              <ul>
                <li><a href="about-us.html">About Us</a></li>
                <li><a href="#">Delivery Information</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms &amp; Conditions</a></li>
                <li><a href="#">Customer Service</a></li>
                <li><a href="#">Return Policy</a></li>
              </ul>
            </div>
          </div>
        </Col>
        <Col lg={4} md={6} sm={6}>
          <div className="footer-widget mb-40">
            <div className="footer-title mb-22">
              <h4>My Account</h4>
            </div>
            <div className="footer-content">
              <ul>
                <li><a href="my-account.html">My Account</a></li>
                <li><a href="#">Order History</a></li>
                <li><a href="wishlist.html">Wish List</a></li>
                <li><a href="#">Newsletter</a></li>
                <li><a href="#">Order History</a></li>
                <li><a href="#">International Orders</a></li>
              </ul>
            </div>
          </div>
        </Col>
        <Col lg={4} md={6} sm={6}>
          <div className="footer-widget mb-40">
            <div className="footer-title mb-22">
              <h4>Get in touch</h4>
            </div>
            <div className="footer-contact">
              <ul>
                <li>Address: 123 Main Your address goes here.</li>
                <li>Telephone Enquiry: (012) 800 456 789-987 </li>
                <li>Email: <a href="#">Info@example.com</a></li>
              </ul>
            </div>
            <div className="mt-35 footer-title mb-22">
              <h4>Get in touch</h4>
            </div>
            <div className="footer-time">
              <ul>
                <li>Open:  <span>8:00 AM</span> - Close: <span>18:00 PM</span></li>
                <li>Saturday - Sunday: <span>Close</span></li>
              </ul>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
  {/* <div className="footer-bottom-area border-top-4">
    <div className="container">
      <div className="row">
        <div className="col-lg-6 col-md-6 col-sm-7">
          <div className="copyright">
            <p>© 2021 <strong> Billy </strong> Made with <i className="fa fa-heart text-danger" /> by <a href="https://hasthemes.com/" target="_blank"><strong>HasThemes</strong></a></p>
          </div>
        </div>
        <div className="col-lg-6 col-md-6 col-sm-5">
          <div className="footer-social">
            <ul>
              <li><a href="#"><i className="ion-social-facebook" /></a></li>
              <li><a href="#"><i className="ion-social-twitter" /></a></li>
              <li><a href="#"><i className="ion-social-instagram-outline" /></a></li>
              <li><a href="#"><i className="ion-social-googleplus-outline" /></a></li>
              <li><a href="#"><i className="ion-social-rss" /></a></li>
              <li><a href="#"><i className="ion-social-dribbble-outline" /></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div> */}
</footer>
);
}

export default Footer