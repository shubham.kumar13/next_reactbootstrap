import React from "react";
import {Carousel, Container} from 'react-bootstrap'

export default function herosection() {
  return (
      <Carousel className="caroulselheight">
      <Carousel.Item className="caroulselheight">
        <img
          className="d-block w-100 img-fluid"
          src="assets/img/slider/slider-2.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item className="caroulselheight">
        <img
          className="d-block w-100 img-fluid"
          src="assets/img/slider/slider-1.jpg"
          alt="Second slide"
        />
        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item className="caroulselheight">
        <img
          className="d-block w-100 img-fluid"
          src="assets/img/slider/slider-2.jpg"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item className="caroulselheight">
        <img
          className="d-block w-100 img-fluid"
          src="assets/img/slider/slider-1.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}
