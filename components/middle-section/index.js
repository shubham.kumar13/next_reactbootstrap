import React from 'react'

export default function middlesection() {
    return (
        <div className="banner-area row-col-decrease pt-100 pb-75 clearfix">
            <div className="container">
                <div className="banner-left-side mb-20">
                <div className="single-banner">
                    <div className="hover-style">
                    <a href="#"><img src="assets/img/banner/banner-1.jpg" alt="" /></a>
                    </div>
                </div>
                </div>
                <div className="banner-right-side mb-20">
                <div className="single-banner mb-20">
                    <div className="hover-style">
                    <a href="#"><img src="assets/img/banner/banner-2.jpg" alt="" /></a>
                    </div>
                </div>
                <div className="single-banner">
                    <div className="hover-style">
                    <a href="#"><img src="assets/img/banner/banner-3.jpg" alt="" /></a>
                    </div>
                </div>
                </div>
            </div>
            </div>

    )
}
