import React from 'react';
import Col from "react-bootstrap";

export default function shoplistfilter() {
    return (
        <div className="col-lg-9">
            <div className="banner-area pb-30">
                <a href="product-details.html"><img alt="" src="assets/img/banner/banner-49.jpg" /></a>
            </div>
            <div className="shop-topbar-wrapper">
                <div className="shop-topbar-left">
                <ul className="view-mode">
                    <li className="active"><a href="#product-grid" data-view="product-grid"><i className="fa fa-th" /></a></li>
                    <li><a href="#product-list" data-view="product-list"><i className="fa fa-list-ul" /></a></li>
                </ul>
                <p>Showing 1 - 20 of 30 results</p>
                </div>
                <div className="product-sorting-wrapper">
                <div className="product-shorting shorting-style">
                    <label>View:</label>
                    <select>
                    <option value> 20</option>
                    <option value> 23</option>
                    <option value> 30</option>
                    </select>
                </div>
                <div className="product-show shorting-style">
                    <label>Sort by:</label>
                    <select>
                    <option value>Default</option>
                    <option value> Name</option>
                    <option value> price</option>
                    </select>
                </div>
                </div>
            </div>
            <div className="grid-list-product-wrapper">
                <div className="product-grid product-view pb-20">
                <div className="row">
                    <div className="product-width col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-1.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-2.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$200.00</span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-3.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$90.00</span>
                            <span className="product-price-old">$100.00 </span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-4.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$50.00</span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-5.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$60.00</span>
                            <span className="product-price-old">$70.00 </span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width pro-list-none col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-6.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$190.00</span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width pro-list-none col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-7.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$150.00</span>
                            <span className="product-price-old">$170.00 </span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width pro-list-none col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-8.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$80.00</span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width pro-list-none col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-9.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$180.00</span>
                            <span className="product-price-old">$190.00 </span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width pro-list-none col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-10.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$70.00</span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width pro-list-none col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-1.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="product-width pro-list-none col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                    <div className="product-wrapper">
                        <div className="product-img">
                        <a href="product-details.html">
                            <img src="assets/img/product/product-2.jpg" alt="" />
                        </a>
                        <div className="product-action">
                            <div className="pro-action-left">
                            <a title="Add Tto Cart" href="#"><i className="ion-android-cart" /> Add Tto Cart</a>
                            </div>
                            <div className="pro-action-right">
                            <a title="Wishlist" href="wishlist.html"><i className="ion-ios-heart-outline" /></a>
                            <a title="Quick View" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#"><i className="ion-android-open" /></a>
                            </div>
                        </div>
                        </div>
                        <div className="product-content">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$200.00</span>
                        </div>
                        </div>
                        <div className="product-list-details">
                        <h4>
                            <a href="product-details.html">PRODUCTS NAME HERE </a>
                        </h4>
                        <div className="product-price-wrapper">
                            <span>$100.00</span>
                            <span className="product-price-old">$120.00 </span>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipic it, sed do eiusmod tempor labor incididunt ut et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        <div className="shop-list-cart-wishlist">
                            <a href="#" title="Wishlist"><i className="ion-ios-heart-outline" /></a>
                            <a href="#" title="Add To Cart"><i className="ion-android-cart" /></a>
                            <a href="#" data-bs-target="#exampleModal" data-bs-toggle="modal" title="Quick View">
                            <i className="ion-android-open" />
                            </a>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div className="pagination-total-pages">
                <div className="pagination-style">
                    <ul>
                    <li><a className="prev-next prev" href="#"><i className="ion-ios-arrow-left" /> Prev</a></li>
                    <li><a className="active" href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">10</a></li>
                    <li><a className="prev-next next" href="#">Next<i className="ion-ios-arrow-right" /> </a></li>
                    </ul>
                </div>
                <div className="total-pages">
                    <p>Showing 1 - 20 of 30 results</p>
                </div>
                </div>
            </div>
            </div>

    )
}

