import React from 'react'

export default function Shortbycategory() {
    return (
        <div className="col-lg-3">
            <div className="shop-sidebar-wrapper gray-bg-7 shop-sidebar-mrg">
                <div className="shop-widget">
                <h4 className="shop-sidebar-title">Shop By Categories</h4>
                <div className="shop-catigory">
                    <ul id="faq">
                    <li> <a data-bs-toggle="collapse" data-parent="#faq" href="#shop-catigory-1">Fast Foods <i className="ion-ios-arrow-down" /></a>
                        <ul id="shop-catigory-1" className="panel-collapse collapse show">
                        <li><a href="#">Pizza </a></li>
                        <li><a href="#">Hamburgers</a></li>
                        <li><a href="#">Sandwiches</a></li>
                        <li><a href="#">French fries</a></li>
                        <li><a href="#">Fried chicken</a></li>
                        </ul>
                    </li>
                    <li> <a data-bs-toggle="collapse" data-parent="#faq" href="#shop-catigory-2">Rich Foods <i className="ion-ios-arrow-down" /></a>
                        <ul id="shop-catigory-2" className="panel-collapse collapse">
                        <li><a href="#">Eggs</a></li>
                        <li><a href="#">Milk</a></li>
                        <li><a href="#">Almonds</a></li>
                        <li><a href="#">Cottage Cheese</a></li>
                        <li><a href="#">Lean Beef</a></li>
                        </ul>
                    </li>
                    <li> <a data-bs-toggle="collapse" data-parent="#faq" href="#shop-catigory-3">soft drinks <i className="ion-ios-arrow-down" /></a>
                        <ul id="shop-catigory-3" className="panel-collapse collapse">
                        <li><a href="#"> Pepsi Cola</a></li>
                        <li><a href="#"> Fruit juices</a></li>
                        <li><a href="#">Diet Pepsi</a></li>
                        <li><a href="#">Bitter lemon</a></li>
                        <li><a href="#">Barley water</a></li>
                        </ul>
                    </li>
                    <li> <a href="#">Vegetables</a> </li>
                    <li> <a href="#">Fruits</a></li>
                    <li> <a href="#">Red Meat</a></li>
                    </ul>
                </div>
                </div>
                <div className="shop-price-filter mt-40 shop-sidebar-border pt-35">
                <h4 className="shop-sidebar-title">Price Filter</h4>
                <div className="price_filter mt-25">
                    <span>Range:  $100.00 - 1.300.00 </span>
                    <div id="slider-range" className="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"><div className="ui-slider-range ui-corner-all ui-widget-header" style={{left: '0%', width: '66.6667%'}} /><span tabIndex={0} className="ui-slider-handle ui-corner-all ui-state-default" style={{left: '0%'}} /><span tabIndex={0} className="ui-slider-handle ui-corner-all ui-state-default" style={{left: '66.6667%'}} /></div>
                    <div className="price_slider_amount">
                    <div className="label-input">
                        <input type="text" id="amount" name="price" placeholder="Add Your Price" />
                    </div>
                    <button type="button">Filter</button> 
                    </div>
                </div>
                </div>
                <div className="shop-widget mt-40 shop-sidebar-border pt-35">
                <h4 className="shop-sidebar-title">By Brand</h4>
                <div className="sidebar-list-style mt-20">
                    <ul>
                    <li><input type="checkbox" /><a href="#">Poure </a></li>
                    <li><input type="checkbox" /><a href="#">Eveman </a></li>
                    <li><input type="checkbox" /><a href="#">Iccaso </a></li>
                    <li><input type="checkbox" /><a href="#">Annopil </a></li>
                    <li><input type="checkbox" /><a href="#">Origina </a></li>
                    <li><input type="checkbox" /><a href="#">Perini</a></li>
                    <li><input type="checkbox" /><a href="#">Dolloz </a></li>
                    <li><input type="checkbox" /><a href="#">Spectry </a></li>
                    </ul>
                </div>
                </div>
                <div className="shop-widget mt-40 shop-sidebar-border pt-35">
                <h4 className="shop-sidebar-title">By Color</h4>
                <div className="sidebar-list-style mt-20">
                    <ul>
                    <li><input type="checkbox" /><a href="#">Black </a></li>
                    <li><input type="checkbox" /><a href="#">Blue </a></li>
                    <li><input type="checkbox" /><a href="#">Green </a></li>
                    <li><input type="checkbox" /><a href="#">Grey </a></li>
                    <li><input type="checkbox" /><a href="#">Red</a></li>
                    <li><input type="checkbox" /><a href="#">White</a></li>
                    <li><input type="checkbox" /><a href="#">Yellow </a></li>
                    </ul>
                </div>
                </div>
                <div className="shop-widget mt-40 shop-sidebar-border pt-35">
                <h4 className="shop-sidebar-title">Compare Products</h4>
                <div className="compare-product">
                    <p>You have no item to compare. </p>
                    <div className="compare-product-btn">
                    <span>Clear all </span>
                    <a href="#">Compare <i className="fa fa-check" /></a>
                    </div>
                </div>
                </div>
                <div className="shop-widget mt-40 shop-sidebar-border pt-35">
                <h4 className="shop-sidebar-title">Popular Tags</h4>
                <div className="shop-tags mt-25">
                    <ul>
                    <li><a href="#">All</a></li>
                    <li><a href="#">Cheesy</a></li>
                    <li><a href="#">Fast Food</a></li>
                    <li><a href="#">French Fries</a></li>
                    <li><a href="#">Hamburger </a></li>
                    <li><a href="#">Pizza</a></li>
                    <li><a href="#">Red Meat</a></li>
                    </ul>
                </div>
                </div>
            </div>
        </div>

    )
}
