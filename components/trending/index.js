import React from "react";

import { Container, Row, Col, Card, Button } from "react-bootstrap";

export default function trending() {
  return (
    <Container>
      <Row className="py-5">
        <Col xs lg="3">
          <Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src="assets/img/product/product-5.jpg" />
            <Card.Body>
              <Card.Title>Card Title</Card.Title>
              <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
              </Card.Text>
              <Button variant="primary">Go somewhere</Button>
            </Card.Body>
          </Card>
        </Col>
        <Col xs lg="3">
          <Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src="assets/img/product/product-1.jpg" />
            <Card.Body>
              <Card.Title>Card Title</Card.Title>
              <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
              </Card.Text>
                <Card.Link href="#">Card Link</Card.Link>
                <Card.Link href="#">Another Link</Card.Link>
            </Card.Body>
          </Card>
        </Col>
        <Col xs lg="3">
          <Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src="assets/img/product/product-3.jpg" />
            <Card.Body>
              <Card.Title>Card Title</Card.Title>
              <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
              </Card.Text>
              <Button variant="primary">Go somewhere</Button>
            </Card.Body>
          </Card>
        </Col>
        <Col xs lg="3">
          <Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src="assets/img/product/product-2.jpg" />
            <Card.Body>
              <Card.Title>Card Title</Card.Title>
              <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
              </Card.Text>
              <Button variant="primary">Go somewhere</Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
