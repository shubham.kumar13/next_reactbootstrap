import React from "react";
import HeroSection from "../components/herosection";
import Trending from "../components/trending";
import Middlesection from "../components/middle-section";
function Home() {
  return (
    <>
    <HeroSection/>
    <Middlesection/>
    <Trending/>
    </>
  )
}

export default Home