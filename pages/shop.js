import React from "react";
import { Container, Row } from "react-bootstrap";
import Shoplistfilter from "../components/shoplistfilter";
import Shortbycategory from "../components/shortbycategory";

export default function shop() {
  return (
    <>
    <div className="breadcrumb-area gray-bg">
      <div className="container">
        <div className="breadcrumb-content">
          <ul>
            <li><a href="index.html">Home</a></li>
            <li className="active">Shop Grid Style </li>
          </ul>
        </div>
      </div>
    </div>
    <div className="shop-page-area pt-100 pb-100">
      <Container>
        <Row className="flex-row-reverse">
          <Shortbycategory/>
          <Shoplistfilter/>
        </Row>
      </Container>
    </div>
    </>
  )
}
